package info.jarek.errorservice.buisness;

import info.jarek.employeeservice.error.Ack;
import info.jarek.employeeservice.error.ErrorMessage;
import info.jarek.employeeservice.error.ErrorServiceGrpc;
import io.grpc.stub.StreamObserver;
import org.lognet.springboot.grpc.GRpcService;

import java.util.logging.Logger;


@GRpcService
public class BuisnessErrorService extends ErrorServiceGrpc.ErrorServiceImplBase {
    private final Logger log = Logger.getLogger(BuisnessErrorService.class.getName());

    @Override
    public void errorStream(ErrorMessage request, StreamObserver<Ack> responseObserver) {
        log.info("onNext from server");
        log.info(request.getMessage());
        log.info(request.getInstance());
        responseObserver.onNext(Ack.newBuilder().setStatus(true).build());
        responseObserver.onCompleted();
    }

}
