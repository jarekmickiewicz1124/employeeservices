# EmployeeServices

Project consists of two spring boot applications:
- employee-service
- error-service 

To build docker images locally run
`./gradlew buildDocker`
