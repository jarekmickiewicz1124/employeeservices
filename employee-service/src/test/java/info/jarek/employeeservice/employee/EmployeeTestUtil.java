package info.jarek.employeeservice.employee;

import info.jarek.employeeservice.address.Address;

import java.util.Arrays;
import java.util.stream.Collectors;

public class EmployeeTestUtil {
    static Employee createTestEntity(String pesel) {

        Address a1 = getTestAddress("home");
        Address a2 = getTestAddress("office");
        Employee e1 = new Employee();
        e1.setAddresses(Arrays.asList(a1, a2).stream().collect(Collectors.toSet()));
        e1.setAge(24);
        e1.setFirstName("Jarek");
        e1.setLastName("Mickiewicz");
        e1.setPesel(pesel);
        e1.setPosition(Position.WORKER);
        return e1;

    }

    static Address getTestAddress(String asd) {
        Address a1 = new Address();

        a1.setAddress1(asd);
        a1.setAddress2("asd2");
        a1.setCity("wroclaw");
        a1.setPostcode("59-830");
        a1.setCountry("PL");
        return a1;
    }

    /**
     * @author Rafal Sadkowski, rafal.sadkowski@gmail.com, http://sadkowski.org
     */
    public static class Pesel {

        private static boolean KOBIETA = true;
        private static boolean MEZCZYZNA = false;
        private static int PESEL_LENGHT = 11;

        public static String[] generatePesels(int year, int mounth, int day, boolean plec, int ilosc) {
            String[] pesels = new String[ilosc];
            int[] peselCurrent = new int[PESEL_LENGHT];
            char[] peselPrint = new char[PESEL_LENGHT];
            int r;
            ilosc *= 2;
            int plecNr = 0;
            if (plec == MEZCZYZNA)
                plecNr++;
            int validMonthNr;
            int validNumber;
            peselCurrent[0] = year / 10 % 10;
            peselCurrent[1] = year % 10;

            if (year < 1800) {
                throw new RuntimeException(
                        "The date is before 1800. Please enter older date");

            } else if (year < 1900) {
                peselCurrent[2] = mounth / 10 + 8;

            } else if (year < 2000) {
                peselCurrent[2] = mounth / 10;
            } else if (year < 2100) {
                peselCurrent[2] = mounth / 10 + 2;
            } else if (year < 2200) {
                peselCurrent[2] = mounth / 10 + 4;
            } else if (year < 2300) {
                peselCurrent[2] = mounth / 10 + 6;
            } else {

                throw new RuntimeException(
                        "The date is after 2300. Please enter earlier date");
            }
            peselCurrent[3] = mounth % 10;
            peselCurrent[4] = day / 10;
            peselCurrent[5] = day % 10;

            //generate a validNumber
            validMonthNr = peselCurrent[0] + peselCurrent[4] + 3 * (peselCurrent[1] + peselCurrent[5]) + 7 * peselCurrent[2] + 9 * peselCurrent[3];
            for (int l = 0; l < 6; l++) {
                peselPrint[l] = (char) (peselCurrent[l] + 48);
            }
            for (int i = 0 + plecNr; i < ilosc; i = i + 2) {
                peselCurrent[6] = i / 1000;
                r = i % 1000;
                peselCurrent[7] = r / 100;
                r = r % 100;
                peselCurrent[8] = r / 10;
                peselCurrent[9] = r % 10;

                validNumber = peselCurrent[8] + 3 * peselCurrent[9] + 7 * peselCurrent[6] + 9 * peselCurrent[7] + validMonthNr;
                validNumber %= 10;
                if (validNumber == 0)
                    peselPrint[10] = 48;
                else
                    peselPrint[10] = (char) (/*10-valid+48)*/58 - validNumber);
                //end generate a valideNumber

                for (int l = 6; l < 10; l++) {
                    peselPrint[l] = (char) (peselCurrent[l] + 48);
                }
                pesels[(i - plecNr) / 2] = String.valueOf(peselPrint);
            }
            return pesels;
        }


    }
}
