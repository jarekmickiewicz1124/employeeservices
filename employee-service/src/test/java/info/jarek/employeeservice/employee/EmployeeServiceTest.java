package info.jarek.employeeservice.employee;

import info.jarek.employeeservice.EmployeeServiceApplication;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;
import java.util.stream.Collectors;

import static info.jarek.employeeservice.employee.EmployeeTestUtil.createTestEntity;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest(classes = EmployeeServiceApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Transactional
class EmployeeServiceTest {
    @Autowired
    private EmployeeRepository employeeRepository;

    @Autowired
    private EmployeeService employeeService;

    @Test
    void save() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 3);
        Employee e1 = createTestEntity(pesels[0]);
        Employee e2 = createTestEntity(pesels[1]);
        Employee e3 = createTestEntity(pesels[2]);

        e1 = employeeService.save(e1);

        e2.setSuperior(e1);
        e2 = employeeService.save(e2);
        e3.setSuperior(e2);
        e3 = employeeService.save(e3);

        List<Employee> all = employeeService.findAll(PageRequest.of(0, 10)).get().collect(Collectors.toList());
        assertEquals(all.size(), 3);
    }

    @Test
    void thorwsBuissnesExcpetionWhenThereIsPeselCollision() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 3);
        Employee e1 = createTestEntity(pesels[0]);
        Employee e2 = createTestEntity(pesels[0]);

        employeeService.save(e1);
        Assertions.assertThrows(BuisnessException.class, () -> employeeService.save(e2));
    }

    @Test
    void throwsBuissnesExcpetionWhenThereAreMoreThan5Suboridantes() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 8);
        Assertions.assertThrows(BuisnessException.class, () -> {
            Set<Employee> employeeSet = new HashSet<>();
            for (int i = 1; i < 6; i++) {
                Employee employee = createTestEntity(pesels[i]);
                final Employee save = employeeService.save(employee);
                employeeSet.add(save);
            }

            Employee manager = createTestEntity(pesels[0]);
            manager.setPosition(Position.MANAGER);
            manager.setSubordinates(employeeSet);
            manager = employeeService.save(employeeService.save(manager));

            Employee employee = createTestEntity(pesels[7]);
            employee.setSuperior(manager);
            employeeService.save(employee);
        });
    }

    @Test
    void throwsBuissnesExcpetionWhenThereAreMoreThan5Chairmen() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 6);
        Assertions.assertThrows(BuisnessException.class, () -> {
            for (int i = 0; i < 6; i++) {
                Employee employee = createTestEntity(pesels[i]);
                employee.setPosition(Position.CHAIRMAN);
                employeeService.save(employee);
            }
        });
    }

    @Test
    void throwsBuissnesExcpetionWhenThereAreMoreThan5SuboridantesFromRoot() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 7);
        Assertions.assertThrows(BuisnessException.class, () -> {
            Set<Employee> employeeSet = new HashSet<>();
            for (int i = 1; i < 7; i++) {
                Employee employee = createTestEntity(pesels[i]);
                final Employee save = employeeService.save(employee);
                employeeSet.add(save);
            }

            Employee manager = createTestEntity(pesels[0]);
            manager.setPosition(Position.MANAGER);
            manager.setSubordinates(employeeSet);
            employeeService.save(manager);
        });

    }

    @Test
    void whenSuperiourIsDeletedChildrenShouldReflectThat() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 3);
        Employee e1 = createTestEntity(pesels[0]);
        Employee e2 = createTestEntity(pesels[1]);
        Employee e3 = createTestEntity(pesels[2]);

        e2.setSuperior(e1);
        e3.setSuperior(e1);

        e1 = employeeRepository.save(e1);
        e2 = employeeRepository.save(e2);
        e3 = employeeRepository.save(e3);

        employeeService.delete(e1.getId());

        Optional<Employee> one = employeeService.findOne(e2.getId());
        Optional<Employee> two = employeeService.findOne(e3.getId());

        assertNull(one.orElseThrow().getSuperior());
        assertNull(two.orElseThrow().getSuperior());
    }

    @Test
    void whenChildIsDeletedSuperiourShouldReflectThat() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 3);

        Employee e1 = createTestEntity(pesels[0]);
        Employee e2 = createTestEntity(pesels[1]);

        e2.setSuperior(e1);

        e1 = employeeRepository.save(e1);
        e2 = employeeRepository.save(e2);

        employeeService.delete(e2.getId());

        Optional<Employee> one = employeeService.findOne(e1.getId());

        assertTrue(one.orElseThrow().getSubordinates().isEmpty());
    }


    @Test
    void whenSubordinatesAreUpdatedSuperiorInThemShouldChanche() {
        String[] pesels = EmployeeTestUtil.Pesel.generatePesels(1995, 1, 1, true, 3);

        Employee e1 = createTestEntity(pesels[0]);
        Employee e2 = createTestEntity(pesels[1]);
        e2 = employeeService.save(e2);
        e1.setSubordinates(Collections.singleton(e2));
        e1 = employeeService.save(e1);

        Optional<Employee> one = employeeService.findOne(e1.getSubordinates().iterator().next().getId());

        assertEquals(one.orElseThrow().getSuperior().getId(), e1.getId());

        employeeService.delete(e1.getId());

        Optional<Employee> two = employeeService.findOne(e2.getId());
        assertNull(two.get().getSuperior());


    }
}
