package info.jarek.employeeservice.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@RestController
@RequestMapping("/api/employee")
public class EmployeeController {

    private final Logger log = LoggerFactory.getLogger(EmployeeService.class);

    private final EmployeeService employeeService;
    private final EmployeeCustomMapper employeeCustomMapper;

    public EmployeeController(EmployeeService employeeService, EmployeeCustomMapper employeeCustomMapper) {
        this.employeeService = employeeService;
        this.employeeCustomMapper = employeeCustomMapper;
    }


    @GetMapping
    public Page<EmployeeDTO> getAll(@NotNull final Pageable pageable) {
        return employeeService.findAll(pageable).map(employeeCustomMapper::entityToDto);
    }

    @GetMapping("testExcp")
    public String getAll() {
        throw new BuisnessException("exception");
    }

    @PostMapping
    public ResponseEntity<EmployeeDTO> addOne(@RequestBody @Valid EmployeeDTO employee) {
        return ResponseEntity.ok(employeeCustomMapper.entityToDto(employeeService.save(employeeCustomMapper.DtoToEntity(employee))));
    }

    @DeleteMapping
    public ResponseEntity<Boolean> deleteOne(@RequestParam Long id) {
        employeeService.delete(id);
        return ResponseEntity.ok(true);
    }

    @PutMapping
    public ResponseEntity<EmployeeDTO> updateOne(@RequestBody EmployeeDTO updatedEmployee) {
        Employee saved = employeeService.save(employeeCustomMapper.DtoToEntity(updatedEmployee));
        return ResponseEntity.ok(employeeCustomMapper.entityToDto(saved));
    }
}
