package info.jarek.employeeservice.employee;


import info.jarek.employeeservice.address.Address;
import org.hibernate.validator.constraints.pl.PESEL;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.HashSet;
import java.util.Set;


/**
 * A Employee.
 */
@Entity
@Table(name = "employee")
public class Employee {
    public Employee() {
    }

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @NotNull
    @Column(name = "first_name", nullable = false)
    @Size(min = 3, max = 100)
    private String firstName;

    @NotNull
    @Column(name = "last_name", nullable = false)
    @Size(min = 3, max = 100)
    private String lastName;

    @NotNull
    @Column(name = "age", nullable = false)
    @Min(18)
    private Integer age;

    @NotNull
    @Column(name = "pesel", nullable = false, unique = true)
    @PESEL
    private String pesel;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "role", nullable = false)
    private Position position;

    @ElementCollection
    @Valid
    private Set<Address> addresses = new HashSet<>();

    @OneToMany(mappedBy = "superior")
    @Size(max = 5)
    private Set<Employee> subordinates = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "SUPERIOUR_ID")
    private Employee superior;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public Employee firstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Employee lastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Integer getAge() {
        return age;
    }

    public Employee age(Integer age) {
        this.age = age;
        return this;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getPesel() {
        return pesel;
    }

    public Employee pesel(String pesel) {
        this.pesel = pesel;
        return this;
    }

    public void setPesel(String pesel) {
        this.pesel = pesel;
    }

    public Position getPosition() {
        return position;
    }

    public Employee role(Position position) {
        this.position = position;
        return this;
    }

    public void setPosition(Position position) {
        this.position = position;
    }

    public Set<Address> getAddresses() {
        return addresses;
    }

    public Employee addresses(Set<Address> addresses) {
        this.addresses = addresses;
        return this;
    }

    public Employee addAddress(Address address) {
        this.addresses.add(address);
        return this;
    }

    public Employee removeAddress(Address address) {
        this.addresses.remove(address);
        return this;
    }

    public void setAddresses(Set<Address> addresses) {
        this.addresses = addresses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Employee)) {
            return false;
        }
        return id != null && id.equals(((Employee) o).id);
    }

    @Override
    public int hashCode() {
        return 31;
    }

    public Set<Employee> getSubordinates() {
        return subordinates;
    }

    public void setSubordinates(Set<Employee> subordinates) {
        this.subordinates = subordinates;
    }

    public Employee getSuperior() {
        return superior;
    }

    public void setSuperior(Employee superior) {
        this.superior = superior;
    }

    @Override
    public String toString() {
        return "Employee{" +
                "id=" + id +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                ", pesel='" + pesel + '\'' +
                ", role=" + position +
                ", superior=" + superior +
                '}';
    }
}
