package info.jarek.employeeservice.employee;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.context.request.WebRequest;

@ControllerAdvice
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class EmployeeAdvice {


    private final ErrorResolver errorResolver;

    public EmployeeAdvice(ErrorResolver errorResolver) {
        this.errorResolver = errorResolver;
    }


    @ExceptionHandler(BuisnessException.class)
    public void handler(BuisnessException e, WebRequest webRequest) {
        errorResolver.resolveException(e, webRequest);

    }
}
