package info.jarek.employeeservice.employee;

import com.google.common.collect.Sets;
import org.springframework.stereotype.Component;

import java.util.Set;
import java.util.stream.Collectors;

@Component
public class EmployeeCustomMapper {

    private final EmployeeRepository employeeRepository;

    public EmployeeCustomMapper(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }


    public Employee DtoToEntity(EmployeeDTO employeeDto) {
        Employee employee = new Employee();
        employee.setId(employeeDto.getId());
        employee.setFirstName(employeeDto.getFirstName());
        employee.setLastName(employeeDto.getLastName());
        employee.setPesel(employeeDto.getPesel());
        employee.setAge(employeeDto.getAge());
        employee.setAddresses(employeeDto.getAddresses());
        employee.setPosition(employeeDto.getPosition());

        Long superior = employeeDto.getSuperior();
        if (superior != null) {
            employeeRepository.findById(superior)
                    .ifPresent(employee::setSuperior);
        }

        Set<Long> subordinates = employeeDto.getSubordinates();
        if (subordinates != null && !subordinates.isEmpty()) {
            Iterable<Employee> allById = employeeRepository.findAllById(employeeDto.getSubordinates());
            employee.setSubordinates(Sets.newHashSet(allById));
        }

        return employee;
    }


    public EmployeeDTO entityToDto(Employee employee) {
        EmployeeDTO employeeDTO = new EmployeeDTO();
        employeeDTO.setId(employee.getId());
        employeeDTO.setFirstName(employee.getFirstName());
        employeeDTO.setLastName(employee.getLastName());
        employeeDTO.setPesel(employee.getPesel());
        employeeDTO.setAge(employee.getAge());
        employeeDTO.setAddresses(employee.getAddresses());
        Employee superiorId = employee.getSuperior();
        if (superiorId != null) {
            employeeDTO.setSuperior(superiorId.getId());
        }
        employeeDTO.setPosition(employee.getPosition());
        employeeDTO.setSubordinates(employee.getSubordinates().stream().map(Employee::getId).collect(Collectors.toSet()));

        return employeeDTO;
    }
}
