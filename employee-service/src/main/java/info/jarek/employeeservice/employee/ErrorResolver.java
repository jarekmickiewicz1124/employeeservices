package info.jarek.employeeservice.employee;

import info.jarek.employeeservice.error.ErrorMessage;
import info.jarek.employeeservice.error.ErrorServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.WebRequest;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

@Service
public class ErrorResolver {
    private final Logger log = Logger.getLogger(ErrorResolver.class.getName());

    private final ErrorServiceGrpc.ErrorServiceFutureStub stub;
    final ExecutorService executor = Executors.newSingleThreadExecutor();


    public ErrorResolver(@Value("${error-service.grpc.port:6565}") Integer port, @Value("${error-service.grpc.host:localhost}") String host) {
        final ManagedChannel channel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build();
        this.stub = ErrorServiceGrpc.newFutureStub(channel);

    }

    public void resolveException(BuisnessException e, WebRequest webRequest) {
        final ErrorMessage errorMessage = ErrorMessage.newBuilder().setMessage(e.getMessage()).setInstance(webRequest.getSessionId()).build();
        stub.errorStream(errorMessage).addListener(() -> log.info("error resolved"), executor);
        ;
    }
}
