package info.jarek.employeeservice.employee;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface EmployeeRepository extends PagingAndSortingRepository<Employee, java.lang.Long> {

    Collection<Employee> findAllBySuperior(Employee sup);

    Collection<Employee> findAllBySubordinatesContains(Employee sub);

    Collection<Employee> findAllByPesel(String pesel);

    Collection<Employee> findAllByPosition(Position position);

}
