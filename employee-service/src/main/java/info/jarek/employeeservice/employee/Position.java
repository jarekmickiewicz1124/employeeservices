package info.jarek.employeeservice.employee;

public enum Position {
    MANAGER, CEO, WORKER, CHAIRMAN
}

