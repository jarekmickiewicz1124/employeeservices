package info.jarek.employeeservice.employee;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;

@Service
@Transactional
public class EmployeeService {
    private static final int MAX_NUMBER_OF_CHAIRMEN = 5;
    private static final int MAX_NUMBER_OF_MANAGERS_SUBORIDNATES = 5;

    private final Logger log = LoggerFactory.getLogger(EmployeeService.class);

    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    /**
     * Save a employee.
     *
     * @param employee the entity to save.
     * @return the persisted entity.
     */
    public Employee save(Employee employee) {
        log.debug("Request to save Employee : {}", employee);

        checkPESELCollision(employee);
        checkOnly5Chairmen(employee);
        checkWorkerIsNotOverLimitForManager(employee);

        Set<Employee> subordinates = employee.getSubordinates();

        if (subordinates != null && !subordinates.isEmpty()) {
            subordinates.forEach(sub -> {
                sub.setSuperior(employee);
            });
            checkManagerHasLessThan5Subs(employee);
        }
        return employeeRepository.save(employee);
    }

    private void checkWorkerIsNotOverLimitForManager(Employee employee) {
        if (employee.getPosition() == Position.WORKER) {
            final Employee superior = employee.getSuperior();
            if (superior != null && superior.getSubordinates().size() >= MAX_NUMBER_OF_MANAGERS_SUBORIDNATES) {
                throw new BuisnessException("MANAGER" + employee.getId() + " has already 5 subordinates");
            }
        }
    }

    private void checkOnly5Chairmen(Employee employee) {
        if (employee.getPosition() == Position.CHAIRMAN) {
            final Collection<Employee> allByPosition = employeeRepository.findAllByPosition(Position.CHAIRMAN);
            final boolean update = allByPosition.stream().anyMatch(x -> x.getId().equals(employee.getId()));
            if (!update) {
                if (allByPosition.size() >= MAX_NUMBER_OF_CHAIRMEN) {
                    throw new BuisnessException("There are already 5 chairmen");
                }
            }
        }

    }

    private void checkManagerHasLessThan5Subs(Employee employee) {
        if (employee.getPosition() == Position.MANAGER && employee.getSubordinates().size() > MAX_NUMBER_OF_MANAGERS_SUBORIDNATES) {
            throw new BuisnessException("MANAGER" + employee.getId() + " has already 5 subordinates");
        }
    }

    private void checkPESELCollision(Employee employee) {
        boolean collision = employeeRepository.findAllByPesel(employee.getPesel()).stream().anyMatch(x -> !x.getId().equals(employee.getId()));
        if (collision) {
            throw new BuisnessException("Pesel collision for employee" + employee.toString());
        }
    }

    /**
     * Get all the employees.
     *
     * @return the list of entities.
     */
    @Transactional(readOnly = true)
    public Page<Employee> findAll(Pageable pageable) {
        log.debug("Request to get all Employees");
        return employeeRepository.findAll(pageable);
    }


    /**
     * Get one employee by id.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    @Transactional(readOnly = true)
    public Optional<Employee> findOne(Long id) {
        log.debug("Request to get Employee : {}", id);
        return employeeRepository.findById(id);
    }

    /**
     * Delete the employee by id.
     *
     * @param id the id of the entity.
     */
    public void delete(Long id) {
        log.debug("Request to delete Employee : {}", id);

        Employee employee = employeeRepository.findById(id).orElseThrow(() -> new IllegalStateException("Cannot remove not existing entity"));
        employeeRepository.findAllBySuperior(employee).forEach(x -> {
            x.setSuperior(null);
            employeeRepository.save(x);
        });

        employeeRepository.findAllBySubordinatesContains(employee).forEach(x -> {
            x.getSubordinates().remove(employee);
            employeeRepository.save(x);
        });

        employeeRepository.deleteById(id);
    }
}
