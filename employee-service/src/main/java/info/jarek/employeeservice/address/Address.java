package info.jarek.employeeservice.address;


import com.sun.istack.NotNull;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.Size;
import java.util.Objects;

/**
 * A Address.
 */
@Embeddable
public class Address {

    @NotNull
    @Column(name = "address_1", nullable = false)
    private String address1;

    @Column(name = "address_2")
    private String address2;

    @NotNull
    @Column(name = "city", nullable = false)
    private String city;

    @NotNull
    @Size(max = 10)
    @Column(name = "postcode", length = 10, nullable = false)
    private String postcode;

    @NotNull
    @Size(max = 2)
    @Column(name = "country", length = 2, nullable = false)
    private String country;

    @Enumerated(EnumType.STRING)
    @Column(name = "adress_type")
    private AdressType adressType;

    public String getAddress1() {
        return address1;
    }

    public Address address1(String address1) {
        this.address1 = address1;
        return this;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getAddress2() {
        return address2;
    }

    public Address address2(String address2) {
        this.address2 = address2;
        return this;
    }

    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getCity() {
        return city;
    }

    public Address city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPostcode() {
        return postcode;
    }

    public Address postcode(String postcode) {
        this.postcode = postcode;
        return this;
    }

    public void setPostcode(String postcode) {
        this.postcode = postcode;
    }

    public String getCountry() {
        return country;
    }

    public Address country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public AdressType getAdressType() {
        return adressType;
    }

    public Address adressType(AdressType adressType) {
        this.adressType = adressType;
        return this;
    }

    public void setAdressType(AdressType adressType) {
        this.adressType = adressType;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Address address = (Address) o;
        return Objects.equals(address1, address.address1) &&
                Objects.equals(address2, address.address2) &&
                Objects.equals(city, address.city) &&
                Objects.equals(postcode, address.postcode) &&
                Objects.equals(country, address.country) &&
                adressType == address.adressType;
    }

    @Override
    public int hashCode() {
        return Objects.hash(address1, address2, city, postcode, country, adressType);
    }

    @Override
    public String toString() {
        return "Address{" +
                ", address1='" + getAddress1() + "'" +
                ", address2='" + getAddress2() + "'" +
                ", city='" + getCity() + "'" +
                ", postcode='" + getPostcode() + "'" +
                ", country='" + getCountry() + "'" +
                ", adressType='" + getAdressType() + "'" +
                "}";
    }
}

