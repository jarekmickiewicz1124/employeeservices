package info.jarek.employeeservice.address;

public enum AdressType {
    CONTACT, REGISTERED
}
