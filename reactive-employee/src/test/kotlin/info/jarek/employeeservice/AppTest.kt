package info.jarek.employeeservice

import org.junit.jupiter.api.Test
import org.springframework.boot.test.context.SpringBootTest

@SpringBootTest(classes = [App::class])
class AppTest {

    @Test
    fun contextLoads() {

    }

}
