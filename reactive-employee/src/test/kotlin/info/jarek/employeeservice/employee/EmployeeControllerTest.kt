package info.jarek.employeeservice.employee

import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.web.reactive.context.ReactiveWebApplicationContext
import org.springframework.http.MediaType
import org.springframework.test.web.reactive.server.WebTestClient
import reactor.test.StepVerifier
import java.time.Instant
import java.util.*

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class EmployeeControllerTest(
        @Autowired val employeeService: EmployeeService,
        @Autowired val context: ReactiveWebApplicationContext,
        @Autowired val mapper: ObjectMapper,
        @Autowired val repository: EmployeeRepository) {

    @AfterEach
    fun teardown() {
        repository.deleteAll().block()
    }

    val client =
            WebTestClient
                    .bindToApplicationContext(context)
                    .build()

    @Test
    fun returnOneEmployee() {
        val employee = Employee(
                null,
                "jar",
                "mic",
                Instant.now(),
                Pesel.generatePesels(1990, 1, 1, true, 1)[0]!!,
                Collections.singletonList(Address("ulica", city = "wroclaw"))
        )
        employeeService.addOrUpdate(employee).block()

        client.get().uri("/api")
                .exchange().expectStatus().is2xxSuccessful
                .expectBody()
                .jsonPath("\$[0].firstName").isEqualTo("jar")
                .jsonPath("\$[0].lastName").isEqualTo("mic")
    }

    @Test
    fun addOneEmployee() {
        val employee = Employee(
                null,
                "jar",
                "mic",
                Instant.now(),
                Pesel.generatePesels(1990, 1, 1, true, 1)[0]!!,
                Collections.singletonList(Address("ulica", city = "wroclaw"))
        )

        client.post()
                .uri("/api")
                .contentType(MediaType.APPLICATION_JSON)
                .bodyValue(mapper.writeValueAsString(employee))
                .exchange().expectStatus().is2xxSuccessful
                .expectBody()
                .jsonPath("\$.id").isNotEmpty()
                .jsonPath("\$.firstName").isEqualTo("jar")
    }

    @Test
    fun deleteEmployee() {
        val employee = Employee(
                null,
                "jar",
                "mic",
                Instant.now(),
                Pesel.generatePesels(1990, 1, 1, true, 1)[0]!!,
                Collections.singletonList(Address("ulica", city = "wroclaw"))
        )
        val saved = employeeService.addOrUpdate(employee).block()

        client.delete()
                .uri("/api/${saved!!.id}")
                .exchange().expectStatus().is2xxSuccessful

        StepVerifier.create(employeeService.findOne(saved.id!!))
                .expectComplete()
                .verify()
    }
}
