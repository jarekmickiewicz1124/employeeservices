package info.jarek.employeeservice.employee

/**
 * @author Rafal Sadkowski, rafal.sadkowski@gmail.com, http://sadkowski.org
 *
 */
object Pesel {
    private const val KOBIETA = true
    private const val MEZCZYZNA = false
    private const val PESEL_LENGHT = 11
    fun generatePesels(year: Int, mounth: Int, day: Int, plec: Boolean, ilosc: Int): Array<String?> {
        var ilosc = ilosc
        val pesels = arrayOfNulls<String>(ilosc)
        val peselCurrent = IntArray(PESEL_LENGHT)
        val peselPrint = CharArray(PESEL_LENGHT)
        var r: Int
        ilosc *= 2
        var plecNr = 0
        if (plec == MEZCZYZNA) plecNr++
        val validMonthNr: Int
        var validNumber: Int
        peselCurrent[0] = year / 10 % 10
        peselCurrent[1] = year % 10
        if (year < 1800) {
            throw RuntimeException(
                    "The date is before 1800. Please enter older date")
        } else if (year < 1900) {
            peselCurrent[2] = mounth / 10 + 8
        } else if (year < 2000) {
            peselCurrent[2] = mounth / 10
        } else if (year < 2100) {
            peselCurrent[2] = mounth / 10 + 2
        } else if (year < 2200) {
            peselCurrent[2] = mounth / 10 + 4
        } else if (year < 2300) {
            peselCurrent[2] = mounth / 10 + 6
        } else {
            throw RuntimeException(
                    "The date is after 2300. Please enter earlier date")
        }
        peselCurrent[3] = mounth % 10
        peselCurrent[4] = day / 10
        peselCurrent[5] = day % 10

        //generate a validNumber
        validMonthNr = peselCurrent[0] + peselCurrent[4] + 3 * (peselCurrent[1] + peselCurrent[5]) + 7 * peselCurrent[2] + 9 * peselCurrent[3]
        for (l in 0..5) {
            peselPrint[l] = (peselCurrent[l] + 48).toChar()
        }
        var i = 0 + plecNr
        while (i < ilosc) {
            peselCurrent[6] = i / 1000
            r = i % 1000
            peselCurrent[7] = r / 100
            r = r % 100
            peselCurrent[8] = r / 10
            peselCurrent[9] = r % 10
            validNumber = peselCurrent[8] + 3 * peselCurrent[9] + 7 * peselCurrent[6] + 9 * peselCurrent[7] + validMonthNr
            validNumber %= 10
            if (validNumber == 0) peselPrint[10] = 48.toChar() else peselPrint[10] = ( /*10-valid+48)*/58 - validNumber).toChar()
            //end generate a valideNumber
            for (l in 6..9) {
                peselPrint[l] = (peselCurrent[l] + 48).toChar()
            }
            pesels[(i - plecNr) / 2] = String(peselPrint)
            i = i + 2
        }
        return pesels
    }
}
