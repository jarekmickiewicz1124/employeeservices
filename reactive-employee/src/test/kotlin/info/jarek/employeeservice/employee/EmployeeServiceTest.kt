package info.jarek.employeeservice.employee

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.test.StepVerifier
import java.time.Instant

@SpringBootTest
class EmployeeServiceTest(@Autowired val service: EmployeeService, @Autowired val repository: EmployeeRepository) {


    @AfterEach
    fun teardown() {
        repository.deleteAll().block()
    }

    @Test
    fun whenFiveChairmenAddedThrowException() {
        val e = Employee(
                null,
                "jar",
                "mic",
                Instant.now(),
                "123123123",
                emptyList(),
                null,
                Position.CHAIRMAN
        )

        val pesels = Pesel.generatePesels(2020, 1, 1, true, 7)


        for (pesel in 0 until MAX_NUMBER_OF_CHAIRMEN) {
            val employee = service.addOrUpdate(e.copy(pesel = pesels[pesel.toInt()]!!, position = Position.CHAIRMAN))

            StepVerifier.create(employee)
                    .expectSubscription()
                    .expectNextCount(1)
                    .expectComplete()
                    .verify()
        }
        val employee = service.addOrUpdate(e.copy(pesel = pesels[MAX_NUMBER_OF_CHAIRMEN.toInt()]!!, position = Position.CHAIRMAN))

        StepVerifier.create(employee)
                .expectSubscription()
                .expectError(BuisnessException::class.java)
                .verify()

    }

    @Test
    fun whenManagerHasMoreThan5SubordinatesThrow() {
        val pesels = Pesel.generatePesels(2020, 1, 1, true, 7)


        val e = Employee(
                null,
                "jar",
                "mic",
                Instant.now(),
                pesels[6]!!,
                emptyList(),
                null,
                Position.MANAGER
        )


        val saved = service.addOrUpdate(e).block()

        for (pesel in 0 until MAX_NUMBER_OF_MANAGERS_SUBORIDNATES) {
            val employee = service.addOrUpdate(e.copy(pesel = pesels[pesel.toInt()]!!, parent = saved, position = Position.WORKER))

            StepVerifier.create(employee)
                    .expectSubscription()
                    .expectNextCount(1)
                    .expectComplete()
                    .verify()
        }
        val employee = service.addOrUpdate(e.copy(pesel = pesels[MAX_NUMBER_OF_MANAGERS_SUBORIDNATES.toInt()]!!, parent = saved, position = Position.WORKER))

        StepVerifier.create(employee)
                .expectSubscription()
                .expectError(BuisnessException::class.java)
                .verify()


    }

    @Test
    fun whenParentIsDeletedChildrenShouldBeUpdated() {
        val pesels = Pesel.generatePesels(2020, 10, 1, true, 2)
        val e = Employee(
                null,
                "jarek",
                "mic",
                Instant.now(),
                pesels[0]!!,
                listOf(Address(address1 = "sien", city = "wrocław"))
        )

        val parent = service.addOrUpdate(e).block()

        val child = Employee(
                null,
                "grzseiek",
                "mic",
                Instant.now(),
                pesels[1]!!,
                listOf(Address(address1 = "sien", city = "wrocław")),
                parent
        )
        val savedChild = service.addOrUpdate(child).block()


        service.delete(parent!!.id!!).block()



        StepVerifier
                .create(service.findOne(savedChild!!.id!!))
                .consumeNextWith { x -> assert(x.parent == null) }
                .verifyComplete()

    }

}
