package info.jarek.employeeservice.employee

import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import reactor.test.StepVerifier
import java.time.Instant

@SpringBootTest
class EmployeeRepositoryTest(
        @Autowired
        val employeeRepository: EmployeeRepository) {


    @AfterEach
    fun teardown() {
        employeeRepository.deleteAll().block()
    }

    @Test
    fun expectEmptyDb() {
        val count = employeeRepository.findAll().count()
        StepVerifier
                .create(count)
                .expectFusion()
                .assertNext { x -> assert(x == 0L) }
                .verifyComplete()
    }

    @Test
    fun countByParent() {
        val e = Employee(
                null,
                "jarek",
                "mic",
                Instant.now(),
                "123456789",
                listOf(Address(address1 = "sien", city = "wrocław")),
                position = Position.MANAGER
        )
        val parent = employeeRepository.save(e).block()
        val child = employeeRepository.save(e.copy(pesel = "1", parent = parent)).block()

        val countOne = employeeRepository.countAllByParentIdAndPeselIsNot(parent!!.id!!, "NONE")
        val countZero = employeeRepository.countAllByParentIdAndPeselIsNot(parent!!.id!!, child!!.pesel)

        StepVerifier
                .create(countOne)
                .assertNext { x -> assert(x == 1L) }
                .verifyComplete()
        StepVerifier
                .create(countZero)
                .assertNext { x -> assert(x == 0L) }
                .verifyComplete()
    }

    @Test
    fun insertOne() {
        val e = Employee(
                null,
                "jarek",
                "mic",
                Instant.now(),
                "123456789",
                listOf(Address(address1 = "sien", city = "wrocław"))
        )

        val saved = employeeRepository.save(e)
        StepVerifier
                .create(saved)
                .expectFusion()
                .assertNext { x -> assert(x.id != null) }
                .verifyComplete()
    }

    @Test
    fun insertTree() {
        val e = Employee(
                null,
                "jarek",
                "mic",
                Instant.now(),
                "123456789",
                listOf(Address(address1 = "sien", city = "wrocław"))
        )

        val saved = employeeRepository.save(e)
                .map {
                    Employee(
                            null,
                            "grzseiek",
                            "mic",
                            Instant.now(),
                            "123456789",
                            listOf(Address(address1 = "sien", city = "wrocław")),
                            it
                    )

                }.flatMap { employeeRepository.save(it) }
        StepVerifier
                .create(saved)
                .expectFusion()
                .assertNext { x -> assert(x.id != null && x.parent?.id != null) }
                .verifyComplete()

    }


}
