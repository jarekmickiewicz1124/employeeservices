package info.jarek.employeeservice.employee

import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.stereotype.Repository
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

const val MAX_NUMBER_OF_CHAIRMEN = 5L
const val MAX_NUMBER_OF_MANAGERS_SUBORIDNATES = 5L

@Repository
interface EmployeeRepository : ReactiveMongoRepository<Employee, String> {
    fun existsByPesel(pesel: String): Mono<Boolean>

    fun countAllByParentIdAndPeselIsNot(parentId: String, pesel: String): Mono<Long>

    fun countAllByPositionAndPeselIsNot(position: Position, pesel: String): Mono<Long>

    fun findAllByParentId(parentId: String): Flux<Employee>
}


@Service
@Transactional
class EmployeeService(val repository: EmployeeRepository) {

    fun addOrUpdate(e: Employee): Mono<Employee> {
        return checkPesel(e)
                .flatMap { checkManager(it) }
                .then(checkChairmen(e))
                .flatMap { repository.save(it) }
    }

    private fun checkPesel(e: Employee): Mono<Employee> {
        return repository.existsByPesel(e.pesel)
                .filter { x -> x == false }
                .switchIfEmpty(Mono.error(BuisnessException("User with same pesel already in DB")))
                .map { e }
    }

    private fun checkManager(e: Employee): Mono<Employee> {

        return if (e.parent != null && e.parent.position == Position.MANAGER) {
            return repository.countAllByParentIdAndPeselIsNot(e.parent.id!!, e.pesel)
                    .filter { it >= MAX_NUMBER_OF_MANAGERS_SUBORIDNATES }
                    .flatMap { Mono.error<Employee>(BuisnessException("To many suboridantes for manager")) }
                    .defaultIfEmpty(e)
        } else {
            Mono.just(e)
        }
    }

    private fun checkChairmen(e: Employee): Mono<Employee> {
        return Mono.just(e.position)
                .filter { x -> x == Position.CHAIRMAN }
                .flatMap { repository.countAllByPositionAndPeselIsNot(Position.CHAIRMAN, e.pesel) }
                .flatMap { x ->
                    if (x >= MAX_NUMBER_OF_CHAIRMEN) {
                        return@flatMap Mono.error<Employee>(BuisnessException("to many chairmen"))
                    } else {
                        return@flatMap Mono.just(e)
                    }
                }
                .defaultIfEmpty(e)
    }

    fun findOne(id: String): Mono<Employee> = repository.findById(id)
    fun getAll(): Flux<Employee> = repository.findAll()
    fun delete(id: String): Mono<Void> {
        return repository.findAllByParentId(id)
                .flatMap { repository.save(it.copy(parent = null)) }
                .then(repository.deleteById(id))

    }

}


@RestController
@RequestMapping("/api")
class EmployeeController(val service: EmployeeService) {

    @GetMapping
    fun getAll(): Flux<Employee> = service.getAll().doOnNext { println(it) }

    @GetMapping("{id}")
    fun getOne(@PathVariable id: String): Mono<Employee> = service.findOne(id)

    @PostMapping
    fun saveOne(@RequestBody e: Employee): Mono<Employee> = service.addOrUpdate(e)

    @DeleteMapping("{id}")
    fun deleteOne(@PathVariable id: String): Mono<Void> = service.delete(id)


}

@ControllerAdvice
class BuisnessExceptionHandler(val errorResolver: ErrorResolver) {
    @ExceptionHandler
    fun handleBuisnessExceptions(e: BuisnessException) {
        errorResolver.resolveException(e)
    }
}
