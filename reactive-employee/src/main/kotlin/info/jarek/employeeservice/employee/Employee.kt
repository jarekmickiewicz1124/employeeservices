package info.jarek.employeeservice.employee

import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Document
import java.time.Instant

@Document
data class Employee(
        @Id
        val id: String?,
        val firstName: String,
        val lastName: String,
        val birthDate: Instant,
        val pesel: String,
        val address: List<Address> = emptyList(),
        val parent: Employee? = null,
        val position: Position = Position.WORKER

)

data class Address(
        val address1: String,
        val address2: String? = null,
        val city: String
)

enum class Position {
        WORKER, MANAGER, CEO, CHAIRMAN
}
