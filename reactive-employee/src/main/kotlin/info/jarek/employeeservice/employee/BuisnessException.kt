package info.jarek.employeeservice.employee

import info.jarek.employeeservice.error.ErrorMessage
import info.jarek.employeeservice.error.ErrorServiceGrpc
import info.jarek.employeeservice.error.ErrorServiceGrpc.ErrorServiceFutureStub
import io.grpc.ManagedChannel
import io.grpc.ManagedChannelBuilder
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Service
import java.util.concurrent.Executors
import java.util.logging.Logger

class BuisnessException(msg: String) : Throwable() {

}


@Service
class ErrorResolver(@Value("\${error-service.grpc.port:6565}") port: Int, @Value("\${error-service.grpc.host:localhost}") host: String) {
    private val log = Logger.getLogger(ErrorResolver::class.java.name)
    private val stub: ErrorServiceFutureStub
    val executor = Executors.newSingleThreadExecutor()

    fun resolveException(e: BuisnessException) {
        val errorMessage = ErrorMessage.newBuilder().setMessage(e.message).build()
        stub.errorStream(errorMessage).addListener(Runnable { log.info("error resolved") }, executor)
    }

    init {
        val channel: ManagedChannel = ManagedChannelBuilder.forAddress(host, port)
                .usePlaintext()
                .build()
        stub = ErrorServiceGrpc.newFutureStub(channel)
    }
}
